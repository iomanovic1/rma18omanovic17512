package com.example.user.spirala1;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by user on 26.3.2018.
 */

public class Knjiga implements Parcelable{
    private String id;
    private String naziv;
    private ArrayList<Autor> autori = new ArrayList<>();
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStranica;

    private Bitmap naslovnaStrana;
    private String autor;

    private String kategorija;
    private Boolean colored;


    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStranica = brojStranica;
        this.autori = autori;
        this.colored = false;

        String authors = "";
        for(int i = 0; i < autori.size(); i++) {
            authors = authors.concat(autori.get(i).getImeIPrezime());
            if(i != autori.size()-1)
                authors = authors.concat("; ");
        }

        this.autor = authors;
    }

    public Knjiga(Bitmap naslovnaStrana, String autor, String naziv, String kategorija) {
        this.naslovnaStrana = naslovnaStrana;
        this.autor = autor;
        this.naziv = naziv;
        this.kategorija = kategorija;
        this.colored = false;
        this.opis = "";
        this.datumObjavljivanja = "";
        this.id = "";
        this.autori = new ArrayList<Autor>();
        this.brojStranica = 0;
    }


    protected Knjiga(Parcel in) {
        id = in.readString();
        naziv = in.readString();
        autori = in.createTypedArrayList(Autor.CREATOR);
        opis = in.readString();
        datumObjavljivanja = in.readString();
        brojStranica = in.readInt();
        naslovnaStrana = in.readParcelable(Bitmap.class.getClassLoader());
        autor = in.readString();
        kategorija = in.readString();
        byte tmpColored = in.readByte();
        colored = tmpColored == 0 ? null : tmpColored == 1;
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };

    public Knjiga() {

    }

    public void setColored(Boolean s) {
        colored = s;
    }

    public Boolean getColored() {
        return colored;
    }

    public Bitmap getNaslovnaStrana() {
        return naslovnaStrana;
    }

    public String getAutor() {
        return autor;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getKategorija() {
        return kategorija;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setNaziv(String naziv){
        this.naziv = naziv;
    }

    public String getOpis(){
        return this.opis;
    }

    public void setOpis(String opis){
        this.opis = opis;
    }

    public void setDatumObjavljivanja(String datum){
        this.datumObjavljivanja = datum;
    }

    public String getDatumObjavljivanja(){
        return  this.datumObjavljivanja;
    }

    public URL getSlika(){
        return this.slika;
    }

    public void setSlika(URL slika){
        this.slika = slika;
    }

    public void setBrojStranica(int brStr){
        this.brojStranica = brStr;
    }

    public int getBrojStranica(){
        return this.brojStranica;
    }

    public void setNaslovnaStr(Bitmap bm){
        this.naslovnaStrana = bm;
    }

    public void setKategorija(String kat){
        this.kategorija = kat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(naziv);
        parcel.writeTypedList(autori);
        parcel.writeString(opis);
        parcel.writeString(datumObjavljivanja);
        parcel.writeInt(brojStranica);
        parcel.writeParcelable(naslovnaStrana, i);
        parcel.writeString(autor);
        parcel.writeString(kategorija);
        parcel.writeByte((byte) (colored == null ? 0 : colored ? 1 : 2));
    }

    public void spojiAutore() {
        String authors = "";
        for(int i = 0; i < autori.size(); i++) {
            authors += ", " + autori.get(i).getImeIPrezime();
            if(i != autori.size()-1)
                authors += "; ";
        }

        this.autor = authors;
    }
}
