package com.example.user.spirala1;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DohvatiNajnovije extends AsyncTask<String , Integer , Void> {

    public interface IDohvatiNajnovijeDone{
        public void onNajnovijeDone(ArrayList<Knjiga> rez);
    }

    ArrayList<Knjiga> rez;

    private IDohvatiNajnovijeDone pozivatelj;
    public DohvatiNajnovije(IDohvatiNajnovijeDone i){
        pozivatelj = i;
        rez = new ArrayList<Knjiga>();
    }

    @Override
    protected Void doInBackground(String... params) {

        String query = null;
        try {
            query = URLEncoder.encode(params[0], "utf-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "https://www.googleapis.com/books/v1/volumes?q=inauthor:" + query + "&orderBy=newest&maxResults=5";

        try {

            URL url = new URL(url1);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");

            for(int i = 0; i < items.length(); i++) {

                JSONObject book = items.getJSONObject(i);

                String id = book.getString("id");

                JSONObject volumeInfo = book.getJSONObject("volumeInfo");
                String naziv = volumeInfo.getString("title");

                ArrayList<Autor> autori = new ArrayList<Autor>();
                if(volumeInfo.has("authors")) {
                    JSONArray autors = volumeInfo.getJSONArray("authors");
                    for(int j = 0; j < autors.length(); j++) {
                        //String imeAutora = autors.getJSONObject(j).toString();
                        String imeAutora = autors.getString(j);
                        autori.add(new Autor(imeAutora, id));
                    }
                }

                String opis = null;
                if(volumeInfo.has("description")) {
                    opis = volumeInfo.getString("description");
                } else {
                    opis = "";
                }

                String datumObjavljivanja = null;
                if(volumeInfo.has("publishedDate")) {
                    datumObjavljivanja = volumeInfo.getString("publishedDate");
                } else {
                    datumObjavljivanja = "";
                }

                JSONObject imageLinks;
                URL slika = null;
                if(volumeInfo.has("imageLinks")) {
                    imageLinks = volumeInfo.getJSONObject("imageLinks");
                    slika = new URL(imageLinks.getString("thumbnail"));
                }

                int brojStranica = 0;
                if(volumeInfo.has("pageCount")) {
                    brojStranica = volumeInfo.getInt("pageCount");
                }

                rez.add(new Knjiga(id, naziv, autori, opis, datumObjavljivanja, slika, brojStranica));

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    static private String convertStreamToString(java.io.InputStream is) {

        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onNajnovijeDone(rez);
    }

}
