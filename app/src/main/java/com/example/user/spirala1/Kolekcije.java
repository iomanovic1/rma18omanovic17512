package com.example.user.spirala1;

import android.app.Application;

import java.util.ArrayList;

/**
 * Created by user on 26.3.2018.
 */

public class Kolekcije extends Application {
    public static ArrayList<Autor> Autori = new ArrayList<Autor>();
    public static ArrayList<String> kategorije = new ArrayList<String>();
    public static ArrayList<Knjiga> Knjige = new ArrayList<>();

    private static Kolekcije kk;

    public static Kolekcije getInstance() {
        return kk;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        kk = this;
    }
}
