package com.example.user.spirala1;

import android.Manifest;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.content.PermissionChecker;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;



public class FragmentPreporuci extends Fragment {

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    ArrayAdapter<String> emailsAdapter;
    final ArrayList<String> imena = new ArrayList<String>();
    final ArrayList<String> emails = new ArrayList<String>();
    Spinner spinner;
    Knjiga knjiga;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Ovdje se dodjeljuje layout fragmentu, tj. šta će se nalaziti unutar fragmenta
        //Ovu liniju ćemo poslije promijeniti
        return inflater.inflate(R.layout.fragment_preporuci, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        knjiga = getArguments().getParcelable("knjiga");
        spinner = (Spinner)getView().findViewById(R.id.sKontakti);

        TextView datumObjavljivanja = (TextView) getView().findViewById(R.id.eDatumObjavljivanja);
        TextView brojStranica = (TextView) getView().findViewById(R.id.eBrojStranica);
        TextView autor = (TextView) getView().findViewById(R.id.eAutor);
        final ImageView slika = (ImageView) getView().findViewById(R.id.eNaslovna);
        TextView naziv = (TextView) getView().findViewById(R.id.eNaziv);
        TextView opis = (TextView) getView().findViewById(R.id.eOpis);

        URL url = knjiga.getSlika();
        if(url != null) {
            Picasso.get().load(url.toString()).into(slika);
        } else {
            slika.setImageBitmap(knjiga.getNaslovnaStrana());
        }

        if(slika.getDrawable() == null) {
            slika.setImageBitmap(((BitmapDrawable)getActivity().getResources().getDrawable(R.drawable.book_launch_graphic2)).getBitmap());
        }

        naziv.setText(knjiga.getNaziv());
        autor.setText(knjiga.getAutor());
        datumObjavljivanja.setText(knjiga.getDatumObjavljivanja());
        opis.setText(knjiga.getOpis());

        String brojStranicaKnjige;
        if(knjiga.getBrojStranica() != 0) {
            brojStranicaKnjige =  String.valueOf(knjiga.getBrojStranica());
        } else {
            brojStranicaKnjige = "Nepoznato";
        }
        brojStranica.setText(brojStranicaKnjige);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && PermissionChecker.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            return;
        } else {
            dodajEmailove();
        }

        emailsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, emails);
        emailsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(emailsAdapter);

        dodajEmailove();
        Button dugmePosalji = (Button) getView().findViewById(R.id.dPosalji);

        dugmePosalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(spinner.getCount() == 0) {
                    return;
                }

                String email = spinner.getSelectedItem().toString();
                int redniBroj = emails.indexOf(email);
                String imeKlijenta = imena.get(redniBroj);
                String imeKnjige = knjiga.getNaziv();
                String imeAutora;

                if(knjiga.getAutori().size() == 0) {
                    imeAutora = knjiga.getAutor();
                } else {
                    imeAutora = knjiga.getAutori().get(0).getImeIPrezime();
                }

                String poruka = "Zdravo " + imeKlijenta + ",\nPročitaj knjigu " + imeKnjige + " od " + imeAutora + "!";

                String[] TO = {email};
                String[] CC = {""};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Preporuka za knjigu");
                emailIntent.putExtra(Intent.EXTRA_TEXT, poruka);
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    getFragmentManager().popBackStack();
                }
                catch (android.content.ActivityNotFoundException ex) {

                }


            }
        });


    }

    private void dodajEmailove() {
        Cursor cursor = null;
        ContentResolver contentResolver = getActivity().getContentResolver();

        Uri uri1 = ContactsContract.Contacts.CONTENT_URI;
        Uri uri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        cursor = contentResolver.query(uri, null, null, null, null);

        while(cursor.moveToNext()) {

            String ime = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            if(email != null){
                imena.add(ime);
                emails.add(email);
            }

        }

        emailsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, emails);
        emailsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(emailsAdapter);

        cursor.close();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Bundle arguments = new  Bundle();
                arguments.putParcelable("knjiga", knjiga);

                FragmentPreporuci fp = new FragmentPreporuci();
                fp.setArguments(arguments);

                KategorijeAkt ka = (KategorijeAkt) getActivity();
                ka.getFragmentManager().beginTransaction().replace(R.id.mjestoF1, fp,"PreporuciKnjigu").addToBackStack(null).commit();
            } else {
                Toast.makeText(getActivity().getBaseContext(), "Until you grant the permission, we canot display the emails", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
