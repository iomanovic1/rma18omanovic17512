package com.example.user.spirala1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class BazaOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "mojaBaza.db";
    public static final int DATABASE_VERSION = 1;

    public static final String KATEGORIJA_ID = "_id";
    public static final String TABELA_KATEGORIJA = "Kategorija";
    public static final String KATEGORIJA_NAZIV = "naziv";

    public static final String KNJIGA_ID = "_id";
    public static final String TABELA_KNJIGA = "Knjiga";
    public static final String KNJIGA_NAZIV = "naziv";
    public static final String KNJIGA_OPIS = "opis";
    public static final String KNJIGA_DATUM_OBJAVLJIVANJA = "datumObjavljivanja";
    public static final String KNJIGA_BROJ_STRANICA = "brojStranica";
    public static final String KNJIGA_ID_WEB_SERVIS = "idWebServis";
    public static final String KNJIGA_ID_KATEGORIJE = "idKategorije";
    public static final String KNJIGA_SLIKA = "slika";
    public static final String KNJIGA_PREGLEDANA = "pregledana";

    public static final String AUTOR_ID = "_id";
    public static final String TABELA_AUTOR = "Autor";
    public static final String AUTOR_IME = "ime";

    public static final String AUTORSTVO_ID = "_id";
    public static final String TABELA_AUTORSTVO = "Autorstvo";
    public static final String AUTORSTVO_ID_AUTORA = "idAutora";
    public static final String AUTORSTVO_ID_KNJIGE = "idKnjige";


    public static final String CREATE_TABLE_KATEGORIJA = "create table " + TABELA_KATEGORIJA + " ("
            + KATEGORIJA_ID + " integer primary key autoincrement, "
            + KATEGORIJA_NAZIV + " text not null unique); ";

    public static final String CREATE_TABLE_KNJIGA = "create table " + TABELA_KNJIGA + " ("
            + KNJIGA_ID + " integer primary key autoincrement, "
            + KNJIGA_NAZIV + " text not null unique, "
            + KNJIGA_OPIS + " text not null, "
            + KNJIGA_DATUM_OBJAVLJIVANJA + " text not null, "
            + KNJIGA_BROJ_STRANICA + " integer not null, "
            + KNJIGA_ID_WEB_SERVIS + " text not null, "
            + KNJIGA_ID_KATEGORIJE + " integer not null, "
            + KNJIGA_SLIKA + " text not null, "
            + KNJIGA_PREGLEDANA + " integer not null); ";

    public static final String CREATE_TABLE_AUTOR = "create table " + TABELA_AUTOR + " ("
            + AUTOR_ID + " integer primary key autoincrement, "
            + AUTOR_IME + " text not null unique); ";

    public static final String CREATE_TABLE_AUTORSTVO = "create table " + TABELA_AUTORSTVO + " ("
            + AUTORSTVO_ID + " integer primary key autoincrement, "
            + AUTORSTVO_ID_AUTORA + " integer not null, "
            + AUTORSTVO_ID_KNJIGE + " integer not null); ";





    public BazaOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_KATEGORIJA);
        db.execSQL(CREATE_TABLE_KNJIGA);
        db.execSQL(CREATE_TABLE_AUTOR);
        db.execSQL(CREATE_TABLE_AUTORSTVO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_KATEGORIJA);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_KNJIGA);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_AUTOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_AUTORSTVO);
        onCreate(db);
    }

    public long dodajKategoriju(String naziv) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues noveVrijednosti = new ContentValues();
        noveVrijednosti.put(BazaOpenHelper.KATEGORIJA_NAZIV, naziv);
        long rowId = db.insert(BazaOpenHelper.TABELA_KATEGORIJA, null, noveVrijednosti);

        return rowId;
    }

    public long dodajKnjigu(Knjiga knjiga) {
        String[] koloneRezulat = new String[]{BazaOpenHelper.KATEGORIJA_ID};
        String where = BazaOpenHelper.KATEGORIJA_NAZIV + " = '" + knjiga.getKategorija() + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.TABELA_KATEGORIJA, koloneRezulat, where, null, null, null, null);

        cursor.moveToFirst();
        String idKategorije = cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_ID));
        cursor.close();

        ContentValues noveVrijednosti = new ContentValues();
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_NAZIV, knjiga.getNaziv());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_OPIS, knjiga.getOpis());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_BROJ_STRANICA, knjiga.getBrojStranica());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_ID_WEB_SERVIS, knjiga.getId());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_ID_KATEGORIJE, idKategorije);
        if(knjiga.getSlika() != null) {
            noveVrijednosti.put(BazaOpenHelper.KNJIGA_SLIKA, knjiga.getSlika().toString());
        } else {
            noveVrijednosti.put(BazaOpenHelper.KNJIGA_SLIKA, "https://ibb.co/nuE7ko");
        }
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_PREGLEDANA, knjiga.getColored());


        long rowIdKnjige = db.insert(BazaOpenHelper.TABELA_KNJIGA, null, noveVrijednosti);

        if(rowIdKnjige == -1)
            return -1;

        if(knjiga.getAutori().size() != 0) {
            for(int i = 0; i < knjiga.getAutori().size(); i++) {
                Autor autor = knjiga.getAutori().get(i);

                ContentValues noveVrijednostiAutora = new ContentValues();
                noveVrijednostiAutora.put(BazaOpenHelper.AUTOR_IME, autor.getImeIPrezime());

                long rowIdAutora = db.insert(BazaOpenHelper.TABELA_AUTOR, null, noveVrijednostiAutora);

                if(rowIdAutora != -1) {

                    ContentValues noveVrijednostiAutorstva = new ContentValues();
                    noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_AUTORA, rowIdAutora);
                    noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_KNJIGE, rowIdKnjige);

                    long rowIdAutorstva = db.insert(BazaOpenHelper.TABELA_AUTORSTVO, null, noveVrijednostiAutorstva);

                } else {
                    String selectionImeAutora = BazaOpenHelper.AUTOR_IME + "='" + autor.getImeIPrezime() + "'";
                    Cursor cursorIdAutora = db.query(BazaOpenHelper.TABELA_AUTOR, null, selectionImeAutora, null, null, null, null);
                    cursorIdAutora.moveToFirst();
                    long idAutora = cursorIdAutora.getLong(cursorIdAutora.getColumnIndex(BazaOpenHelper.AUTOR_ID));

                    ContentValues noveVrijednostiAutorstva = new ContentValues();
                    noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_AUTORA, idAutora);
                    noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_KNJIGE, rowIdKnjige);

                    long rowIdAutorstva = db.insert(BazaOpenHelper.TABELA_AUTORSTVO, null, noveVrijednostiAutorstva);
                }
            }
        } else if(!knjiga.getAutor().contains(";")) {
            String imeAutora = knjiga.getAutor();

            ContentValues noveVrijednostiAutora = new ContentValues();
            noveVrijednostiAutora.put(BazaOpenHelper.AUTOR_IME, imeAutora);

            long rowIdAutora = db.insert(BazaOpenHelper.TABELA_AUTOR, null, noveVrijednostiAutora);

            if(rowIdAutora != -1) {

                ContentValues noveVrijednostiAutorstva = new ContentValues();
                noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_AUTORA, rowIdAutora);
                noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_KNJIGE, rowIdKnjige);

                long rowIdAutorstva = db.insert(BazaOpenHelper.TABELA_AUTORSTVO, null, noveVrijednostiAutorstva);

            } else {
                String selectionImeAutora = BazaOpenHelper.AUTOR_IME + "='" + imeAutora + "'";
                Cursor cursorIdAutora = db.query(BazaOpenHelper.TABELA_AUTOR, null, selectionImeAutora, null, null, null, null);
                cursorIdAutora.moveToFirst();
                long idAutora = cursorIdAutora.getLong(cursorIdAutora.getColumnIndex(BazaOpenHelper.AUTOR_ID));

                ContentValues noveVrijednostiAutorstva = new ContentValues();
                noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_AUTORA, idAutora);
                noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_KNJIGE, rowIdKnjige);

                long rowIdAutorstva = db.insert(BazaOpenHelper.TABELA_AUTORSTVO, null, noveVrijednostiAutorstva);
            }
        }


        return rowIdKnjige;
    }

    public ArrayList<Knjiga> knjigeKategorije(long idKategorije) {

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<Knjiga> knjigeKategorije = new ArrayList<Knjiga>();

        String selection = BazaOpenHelper.KNJIGA_ID_KATEGORIJE + "=" + idKategorije;

        Cursor cursor = db.query(BazaOpenHelper.TABELA_KNJIGA, null, selection, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Knjiga knjiga = new Knjiga();
            knjiga.setNaziv(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV)));
            knjiga.setOpis(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_OPIS)));
            knjiga.setDatumObjavljivanja(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA)));
            knjiga.setBrojStranica(cursor.getInt(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_BROJ_STRANICA)));
            knjiga.setId(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_ID_WEB_SERVIS)));
            knjiga.setAutori(new ArrayList<Autor>());
            try {
                knjiga.setSlika(new URL(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_SLIKA))));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            knjiga.setColored(cursor.getInt(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_PREGLEDANA)) != 0);

            String selectionAutora = BazaOpenHelper.AUTORSTVO_ID_KNJIGE + "=" + cursor.getLong(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_ID));
            Cursor cursorAutori = db.query(BazaOpenHelper.TABELA_AUTORSTVO, null, selectionAutora, null, null, null, null);

            cursorAutori.moveToFirst();
            while(!cursorAutori.isAfterLast()) {

                long idTrenutnogAutora = cursorAutori.getLong(cursorAutori.getColumnIndex(BazaOpenHelper.AUTORSTVO_ID_AUTORA));
                String selectionImenaAutora = BazaOpenHelper.AUTOR_ID + "=" + idTrenutnogAutora;
                Cursor cursorAutor = db.query(BazaOpenHelper.TABELA_AUTOR, null, selectionImenaAutora, null, null, null, null);
                cursorAutor.moveToFirst();
                String imeAutora = cursorAutor.getString(cursorAutor.getColumnIndex(BazaOpenHelper.AUTOR_IME));
                Autor autor = new Autor(imeAutora);

                String selectionKnjigeAutora = BazaOpenHelper.AUTORSTVO_ID_AUTORA + "=" + idTrenutnogAutora;
                Cursor cursorKnjigeAutora = db.query(BazaOpenHelper.TABELA_AUTORSTVO, null, selectionKnjigeAutora, null, null, null, null);

                cursorKnjigeAutora.moveToFirst();
                while(!cursorKnjigeAutora.isAfterLast()) {
                    long idKnjigeAutora = cursorKnjigeAutora.getLong(cursorKnjigeAutora.getColumnIndex(BazaOpenHelper.KNJIGA_ID));
                    String selectionKnjigaAutora = BazaOpenHelper.KNJIGA_ID + "=" + idKnjigeAutora;
                    Cursor cursorKnjigaAutora = db.query(BazaOpenHelper.TABELA_KNJIGA, null, selectionKnjigaAutora, null, null, null, null);

                    if(cursorKnjigaAutora.getCount() != 0) {
                        cursorKnjigaAutora.moveToFirst();
                        autor.getKnjige().add(cursorKnjigaAutora.getString(cursorKnjigaAutora.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV)));
                    }


                    cursorKnjigeAutora.moveToNext();
                }

                knjiga.getAutori().add(autor);
                cursorAutori.moveToNext();
            }

            knjiga.spojiAutore();
            knjigeKategorije.add(knjiga);
            cursor.moveToNext();
        }


        return knjigeKategorije;
    }

    public ArrayList<Knjiga> knjigeAutora(long idAutora) {

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<Knjiga> knjigeAutora = new ArrayList<Knjiga>();

        String selection = BazaOpenHelper.AUTORSTVO_ID_AUTORA + "=" + idAutora;

        Cursor cursor2 = db.query(BazaOpenHelper.TABELA_AUTORSTVO, null, selection, null, null, null, null);

        ArrayList<Long> idKnjiga = new ArrayList<Long>();

        cursor2.moveToFirst();
        while(!cursor2.isAfterLast()) {
            idKnjiga.add(cursor2.getLong(cursor2.getColumnIndex(BazaOpenHelper.AUTORSTVO_ID_KNJIGE)));
            cursor2.moveToNext();
        }

        for(Long idKnjige : idKnjiga) {

            String selectionKnjige = BazaOpenHelper.KNJIGA_ID + "=" + idKnjige;
            Cursor cursor = db.query(BazaOpenHelper.TABELA_KNJIGA, null, selectionKnjige, null, null, null, null);

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                Knjiga knjiga = new Knjiga();
                knjiga.setNaziv(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV)));
                knjiga.setOpis(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_OPIS)));
                knjiga.setDatumObjavljivanja(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA)));
                knjiga.setBrojStranica(cursor.getInt(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_BROJ_STRANICA)));
                knjiga.setId(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_ID_WEB_SERVIS)));
                try {
                    knjiga.setSlika(new URL(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_SLIKA))));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                knjiga.setColored(cursor.getInt(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_PREGLEDANA)) != 0);

                knjiga.setAutori(new ArrayList<Autor>());

                String selectionAutora = BazaOpenHelper.AUTORSTVO_ID_KNJIGE + "=" + idKnjige;
                Cursor cursorAutori = db.query(BazaOpenHelper.TABELA_AUTORSTVO, null, selectionAutora, null, null, null, null);

                cursorAutori.moveToFirst();
                while(!cursorAutori.isAfterLast()) {

                    long idTrenutnogAutora = cursorAutori.getLong(cursorAutori.getColumnIndex(BazaOpenHelper.AUTORSTVO_ID_AUTORA));
                    String selectionImenaAutora = BazaOpenHelper.AUTOR_ID + "=" + idTrenutnogAutora;
                    Cursor cursorAutor = db.query(BazaOpenHelper.TABELA_AUTOR, null, selectionImenaAutora, null, null, null, null);
                    cursorAutor.moveToFirst();
                    String imeAutora = cursorAutor.getString(cursorAutor.getColumnIndex(BazaOpenHelper.AUTOR_IME));
                    Autor autor = new Autor(imeAutora);

                    String selectionKnjigeAutora = BazaOpenHelper.AUTORSTVO_ID_AUTORA + "=" + idTrenutnogAutora;
                    Cursor cursorKnjigeAutora = db.query(BazaOpenHelper.TABELA_AUTORSTVO, null, selectionKnjigeAutora, null, null, null, null);

                    cursorKnjigeAutora.moveToFirst();
                    while(!cursorKnjigeAutora.isAfterLast()) {
                        long idKnjigeAutora = cursorKnjigeAutora.getLong(cursorKnjigeAutora.getColumnIndex(BazaOpenHelper.KNJIGA_ID));
                        String selectionKnjigaAutora = BazaOpenHelper.KNJIGA_ID + "=" + idKnjigeAutora;
                        Cursor cursorKnjigaAutora = db.query(BazaOpenHelper.TABELA_KNJIGA, null, selectionKnjigaAutora, null, null, null, null);

                        if(cursorKnjigaAutora.getCount() != 0) {
                            cursorKnjigaAutora.moveToFirst();
                            autor.getKnjige().add(cursorKnjigaAutora.getString(cursorKnjigaAutora.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV)));
                        }


                        cursorKnjigeAutora.moveToNext();
                    }

                    knjiga.getAutori().add(autor);
                    cursorAutori.moveToNext();
                }

                knjiga.spojiAutore();
                knjigeAutora.add(knjiga);
                cursor.moveToNext();
            }
        }

        return knjigeAutora;
    }

    public ArrayList<Knjiga> dodajSveKnjige() {

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<Knjiga> sveKnjige = new ArrayList<Knjiga>();


        Cursor cursor = db.query(BazaOpenHelper.TABELA_KNJIGA, null, null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Knjiga knjiga = new Knjiga();
            knjiga.setNaziv(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV)));
            knjiga.setOpis(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_OPIS)));
            knjiga.setDatumObjavljivanja(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA)));
            knjiga.setBrojStranica(cursor.getInt(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_BROJ_STRANICA)));
            knjiga.setId(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_ID_WEB_SERVIS)));
            try {
                knjiga.setSlika(new URL(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_SLIKA))));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            knjiga.setColored(cursor.getInt(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_PREGLEDANA)) != 0);

            long idKnjige = cursor.getLong(cursor.getColumnIndex(BazaOpenHelper.KNJIGA_ID));

            knjiga.setAutori(new ArrayList<Autor>());

            String selectionAutora = BazaOpenHelper.AUTORSTVO_ID_KNJIGE + "=" + idKnjige;
            Cursor cursorAutori = db.query(BazaOpenHelper.TABELA_AUTORSTVO, null, selectionAutora, null, null, null, null);

            cursorAutori.moveToFirst();
            while(!cursorAutori.isAfterLast()) {

                long idTrenutnogAutora = cursorAutori.getLong(cursorAutori.getColumnIndex(BazaOpenHelper.AUTORSTVO_ID_AUTORA));
                String selectionImenaAutora = BazaOpenHelper.AUTOR_ID + "=" + idTrenutnogAutora;
                Cursor cursorAutor = db.query(BazaOpenHelper.TABELA_AUTOR, null, selectionImenaAutora, null, null, null, null);
                cursorAutor.moveToFirst();
                String imeAutora = cursorAutor.getString(cursorAutor.getColumnIndex(BazaOpenHelper.AUTOR_IME));
                Autor autor = new Autor(imeAutora);

                String selectionKnjigeAutora = BazaOpenHelper.AUTORSTVO_ID_AUTORA + "=" + idTrenutnogAutora;
                Cursor cursorKnjigeAutora = db.query(BazaOpenHelper.TABELA_AUTORSTVO, null, selectionKnjigeAutora, null, null, null, null);

                cursorKnjigeAutora.moveToFirst();
                while(!cursorKnjigeAutora.isAfterLast()) {
                    long idKnjigeAutora = cursorKnjigeAutora.getLong(cursorKnjigeAutora.getColumnIndex(BazaOpenHelper.KNJIGA_ID));
                    String selectionKnjigaAutora = BazaOpenHelper.KNJIGA_ID + "=" + idKnjigeAutora;
                    Cursor cursorKnjigaAutora = db.query(BazaOpenHelper.TABELA_KNJIGA, null, selectionKnjigaAutora, null, null, null, null);

                    if(cursorKnjigaAutora.getCount() != 0) {
                        cursorKnjigaAutora.moveToFirst();
                        autor.getKnjige().add(cursorKnjigaAutora.getString(cursorKnjigaAutora.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV)));
                    }


                    cursorKnjigeAutora.moveToNext();
                }

                knjiga.getAutori().add(autor);
                cursorAutori.moveToNext();
            }

            knjiga.spojiAutore();
            sveKnjige.add(knjiga);
            cursor.moveToNext();
        }
        return sveKnjige;
    }

    public ArrayList<String> dodajSveKategorije() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.TABELA_KATEGORIJA, null, null, null, null, null, null);

        ArrayList<String> sveKategorije = new ArrayList<String>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            sveKategorije.add(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_NAZIV))); //add the item
            cursor.moveToNext();
        }
        return sveKategorije;
    }

    public ArrayList<Autor> dodajSveAutore() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.TABELA_AUTOR, null, null, null, null, null, null);

        ArrayList<Autor> sviAutori = new ArrayList<Autor>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {

            Autor autor = new Autor();
            autor.setImeiPrezime(cursor.getString(cursor.getColumnIndex(BazaOpenHelper.AUTOR_IME)));
            autor.setKnjige(new ArrayList<String>());

            if(autor.getImeIPrezime().equals("") || autor.getImeIPrezime() == null) {
                cursor.moveToNext();
                continue;
            }

            String selectionKnjige = BazaOpenHelper.AUTORSTVO_ID_AUTORA + "=" + cursor.getLong(cursor.getColumnIndex(BazaOpenHelper.AUTOR_ID));
            Cursor cursorAutorstva = db.query(BazaOpenHelper.TABELA_AUTORSTVO, null, selectionKnjige, null, null, null, null);

            cursorAutorstva.moveToFirst();
            while(!cursorAutorstva.isAfterLast()) {

                Long idKnjige = cursorAutorstva.getLong(cursorAutorstva.getColumnIndex(BazaOpenHelper.AUTORSTVO_ID_KNJIGE));

                String selection = BazaOpenHelper.KNJIGA_ID + "=" + idKnjige;
                Cursor cursorKnjige = db.query(BazaOpenHelper.TABELA_KNJIGA, null, selection, null, null, null, null);
                cursorKnjige.moveToFirst();
                String nazivKnjige = cursorKnjige.getString(cursorKnjige.getColumnIndex(BazaOpenHelper.KNJIGA_NAZIV));

                autor.getKnjige().add(nazivKnjige);
                cursorAutorstva.moveToNext();
            }




            sviAutori.add(autor);
            cursor.moveToNext();
        }
        return sviAutori;
    }
}
