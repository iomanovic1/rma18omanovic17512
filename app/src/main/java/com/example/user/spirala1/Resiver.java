package com.example.user.spirala1;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class Resiver extends ResultReceiver {

    private Receiver mReceiver;

    public Resiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    //deklaracija interfejsa koji će se trebati implementirati
    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if(mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
