package com.example.user.spirala1;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by user on 8.4.2018.
 */

public class Autor implements Parcelable {
    private String imeiPrezime;
    private ArrayList<String> knjige = new ArrayList<>();
    private int brojKnjiga;

    public Autor(){}

    public Autor(String imeIPrezime, int brojKnjiga) {
        this.imeiPrezime = imeIPrezime;
        this.brojKnjiga = brojKnjiga;
    }

    public Autor(String imeIPrezime, String id) {
        this.imeiPrezime = imeIPrezime;
        this.knjige.add(id);
    }

    public Autor(String imeIPrezime) {
        this.imeiPrezime = imeIPrezime;
        this.brojKnjiga = 1;
    }

    protected Autor(Parcel in) {
        imeiPrezime = in.readString();
        brojKnjiga = in.readInt();
    }

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }

    public void dodajKnjigu(String id){
        this.knjige.add(id);
    }

    public static final Creator<Autor> CREATOR = new Creator<Autor>() {
        @Override
        public Autor createFromParcel(Parcel in) {
            return new Autor(in);
        }
        @Override
        public Autor[] newArray(int size) {
            return new Autor[size];
        }
    };

    public String getImeIPrezime() { return imeiPrezime; }

    public int getBrojKnjiga() { return brojKnjiga; }

    public void povecajBrojKnjiga() { brojKnjiga++; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imeiPrezime);
        dest.writeInt(brojKnjiga);
    }
}
