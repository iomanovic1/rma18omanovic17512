package com.example.user.spirala1;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class KategorijeAkt extends AppCompatActivity implements ListeFragment.OnClick, DodavanjeKnjigeFragment.OnClicksDodajKnjigu, KnjigeFragment.OnClicks, FragmentOnline.OnClicksDodajOnline{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);


        setContentView(R.layout.activity_kategorije_akt);

        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);

        Kolekcije.kategorije = bazaOpenHelper.dodajSveKategorije();
        Kolekcije.Knjige = bazaOpenHelper.dodajSveKnjige();
        Kolekcije.Autori = bazaOpenHelper.dodajSveAutore();

        FragmentManager fm = getFragmentManager();
        FrameLayout layoutListe = (FrameLayout)findViewById(R.id.mjestoF1);

        if(layoutListe != null) {
            ListeFragment lf;
            lf = (ListeFragment)fm.findFragmentById(R.id.mjestoF1);

            if(lf == null) {

                lf = new ListeFragment();

                Bundle argumenti = new Bundle();


                argumenti.putStringArrayList("kategorije", Kolekcije.kategorije);
                lf.setArguments(argumenti);

                fm.beginTransaction().replace(R.id.mjestoF1, lf,"ListaKategorije").commit();
            } else {
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }

    }

    @Override
    public void onItemClickedKategorije(int pos){
        FragmentManager fm = getFragmentManager();
        KnjigeFragment kf = (KnjigeFragment)fm.findFragmentByTag("ListaKnjigePoKategoriji");

        if(kf == null) {
            kf = new KnjigeFragment();
            Bundle argumenti = new Bundle();
            argumenti.putString("kategorija", Kolekcije.kategorije.get(pos));
            kf.setArguments(argumenti);
            getFragmentManager().beginTransaction().replace(R.id.mjestoF1, kf,"ListaKnjigePoKategoriji").addToBackStack(null).commit();

        } else {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
    @Override
    public void onItemClickedAutori(int pos){
        FragmentManager fm = getFragmentManager();
        KnjigeFragment kf = (KnjigeFragment)fm.findFragmentByTag("ListaKnjigePoAutoru");

        if(kf == null) {
            kf = new KnjigeFragment();
            Bundle argumenti = new Bundle();
            argumenti.putString("autor", Kolekcije.Autori.get(pos).getImeIPrezime());
            kf.setArguments(argumenti);
            getFragmentManager().beginTransaction().replace(R.id.mjestoF1, kf,"ListaKnjigePoAutoru").
                    addToBackStack(null).commit();
        } else {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onClickDodajKnjiguOnline() {
        Bundle arguments = new  Bundle();
        FragmentOnline fo = new FragmentOnline();
        fo.setArguments(arguments);
        getFragmentManager().beginTransaction().replace(R.id.mjestoF1, fo, "DodajKnjiguOnline").addToBackStack("DodajKnjiguOnline").commit();
    }

    @Override
    public void onClickKategorije(){
        FragmentManager fm = getFragmentManager();
        ListeFragment fl = (ListeFragment)fm.findFragmentByTag("ListaKategorije");
        if(fl == null) {

            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fl = new ListeFragment();

            ArrayList<String> kategorije = Kolekcije.kategorije;


            Bundle argumenti = new Bundle();
            argumenti.putStringArrayList("kategorije", kategorije);
            fl.setArguments(argumenti);

            getFragmentManager().beginTransaction().replace(R.id.mjestoF1, fl,"ListaKategorije").addToBackStack(null).commit();
        } else {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
    @Override
    public void onClickAutori(){
        FragmentManager fm = getFragmentManager();
        ListeFragment fl = (ListeFragment)fm.findFragmentByTag("ListaAutori");



        if(fl == null) {
            fl = new ListeFragment();
            ArrayList<Autor> autori = Kolekcije.Autori;



            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("autori", autori);
            fl.setArguments(argumenti);
            getFragmentManager().beginTransaction().replace(R.id.mjestoF1, fl,"ListaAutori").addToBackStack(null).commit();
        } else {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
    @Override
    public void onClickPretraga(){
        final ArrayAdapter<String> adapter;

        adapter = new ArrayAdapter <String>(this,android.R.layout.simple_list_item_1, Kolekcije.kategorije);

        EditText tekst = (EditText)findViewById(R.id.tekstPretraga);
        final Button dodajKategoriju = (Button)findViewById(R.id.dDodajKategoriju);

        final ListView lista = (ListView)findViewById(R.id.listaKategorija);

        lista.setAdapter(adapter);

        adapter.getFilter().filter(tekst.getText().toString(), new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {
                if(count == 0)
                    dodajKategoriju.setEnabled(true);
            }
        });
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickDodajKategoriju(){
        EditText tekst = (EditText)findViewById(R.id.tekstPretraga);

        if(tekst.getText().toString().isEmpty()) {
            return;
        }

        final ArrayAdapter<String> adapter;

        adapter = new ArrayAdapter <String>(this,android.R.layout.simple_list_item_1, Kolekcije.kategorije);


        final Button dodajKategoriju = (Button)findViewById(R.id.dDodajKategoriju);

        final ListView lista = (ListView)findViewById(R.id.listaKategorija);

        lista.setAdapter(adapter);

        adapter.getFilter().filter("");
        String unijeti = tekst.getText().toString();
        Kolekcije.kategorije.add(unijeti);
        adapter.notifyDataSetChanged();
        tekst.setText("");
        dodajKategoriju.setEnabled(false);
    }
    @Override
    public void onClickDodajKnjigu() {
        Bundle arguments = new  Bundle();
        DodavanjeKnjigeFragment dkf = new DodavanjeKnjigeFragment();
        dkf.setArguments(arguments);
        getFragmentManager().beginTransaction().replace(R.id.mjestoF1, dkf).addToBackStack(null).commit();
    }
    @Override
    public void onClickNadjiSliku() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Uri data = Uri.fromFile(Environment.getExternalStorageDirectory());
        String type = "image/*";
        intent.setDataAndType(data, type);


        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, "Odaberite fotografiju"), 1);
        }

    }
    @Override
    public void onClickPonisti() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }

    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            if(data == null)
                return;

            final TextView nazivKnjige = (TextView) findViewById(R.id.nazivKnjige);
            String nazivSlike = nazivKnjige.getText().toString();
            if(nazivSlike.length() == 0)
                nazivSlike = "...";
            FileOutputStream outputStream;
            outputStream = openFileOutput(nazivSlike, Context.MODE_PRIVATE);
            getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
            outputStream.close();

            ImageView slika = (ImageView)findViewById(R.id.naslovnaStr);
            Bitmap bm = null;
            while(bm == null)
                bm = BitmapFactory.decodeStream(openFileInput(nazivSlike));
            slika.setImageBitmap(bm);

        }
        catch(IOException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onClickPovratak() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
        } else {
            getFragmentManager().popBackStack();
        }

    }


}
