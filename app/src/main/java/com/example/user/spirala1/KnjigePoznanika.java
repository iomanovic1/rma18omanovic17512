package com.example.user.spirala1;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class KnjigePoznanika extends IntentService {
    public static final int STATUS_START = 0;
    public static final int STATUS_FINISH = 1;
    public static final int STATUS_ERROR = 2;

    public KnjigePoznanika() {
        super(null);
    }

    public KnjigePoznanika(String name) {
        super(name);

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    protected void onHandleIntent(Intent intent) {

        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        String idKorisnika = intent.getStringExtra("idKorisnika");
        ArrayList<Knjiga> rez = new ArrayList<Knjiga>();
        receiver.send(STATUS_START, Bundle.EMPTY);

        String query = null;
        try {
            query = URLEncoder.encode(idKorisnika, "utf-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String url1 = "https://www.googleapis.com/books/v1/users/" + query + "/bookshelves";

        try {

            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");

            for (int i = 0; i < items.length(); i++) {

                JSONObject bookshelf = items.getJSONObject(i);
                int idBookshelf = bookshelf.getInt("id");
                String url2 = url1 + "/" + idBookshelf + "/volumes";
                URL urlBookshelf = new URL(url2);
                HttpURLConnection urlConnection2 = (HttpURLConnection) urlBookshelf.openConnection();
                InputStream in2 = new BufferedInputStream(urlConnection2.getInputStream());
                String rezultat2 = convertStreamToString(in2);
                JSONObject jo2 = new JSONObject(rezultat2);
                JSONArray items2 = jo2.getJSONArray("items");

                for(int j = 0; j <items2.length(); j++) {

                    JSONObject book = items2.getJSONObject(j);
                    String id = book.getString("id");

                    JSONObject volumeInfo = book.getJSONObject("volumeInfo");
                    String naziv = volumeInfo.getString("title");

                    ArrayList<Autor> autori = new ArrayList<Autor>();
                    if(volumeInfo.has("authors")) {
                        JSONArray autors = volumeInfo.getJSONArray("authors");
                        for(int k = 0; k < autors.length(); k++) {
                            //String imeAutora = autors.getJSONObject(k).toString();
                            String imeAutora = autors.getString(k);
                            autori.add(new Autor(imeAutora, id));
                        }
                    }

                    String opis = null;
                    if(volumeInfo.has("description")) {
                        opis = volumeInfo.getString("description");
                    } else {
                        opis = "";
                    }

                    String datumObjavljivanja = null;
                    if(volumeInfo.has("publishedDate")) {
                        datumObjavljivanja = volumeInfo.getString("publishedDate");
                    } else {
                        datumObjavljivanja = "";
                    }

                    JSONObject imageLinks;
                    URL slika = null;
                    if(volumeInfo.has("imageLinks")) {
                        imageLinks = volumeInfo.getJSONObject("imageLinks");
                        slika = new URL(imageLinks.getString("thumbnail"));
                    }

                    int brojStranica = 0;
                    if(volumeInfo.has("pageCount")) {
                        brojStranica = volumeInfo.getInt("pageCount");
                    }

                    rez.add(new Knjiga(id, naziv, autori, opis, datumObjavljivanja, slika, brojStranica));

                }


            }

            bundle.putParcelableArrayList("result", rez);
            receiver.send(STATUS_FINISH, bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
        } catch (IOException e) {
            e.printStackTrace();
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
        } catch (JSONException e) {
            e.printStackTrace();
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
        }



    }

    static private String convertStreamToString(java.io.InputStream is) {

        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
