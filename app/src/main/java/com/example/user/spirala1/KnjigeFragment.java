package com.example.user.spirala1;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by user on 8.4.2018.
 */

public class KnjigeFragment extends Fragment{
    OnClicks klik;
    ArrayList<Knjiga> knjige;
    ListView lista;
    MyArrayAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.lista_knjiga_fragment, container, false);

        knjige = new ArrayList<Knjiga>();

        lista = (ListView)view.findViewById(R.id.listaKnjiga);

        adapter = new MyArrayAdapter(view.getContext(),R.layout.element_liste, knjige);

        lista.setAdapter(adapter);

        String kategorija = getArguments().getString("kategorija");
        String autor = getArguments().getString("autor");

        if(getArguments().containsKey("kategorija")) {

            BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity().getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);
            SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
            String selection = BazaOpenHelper.KATEGORIJA_NAZIV + "='" + kategorija + "'";

            Cursor cursor = db.query(BazaOpenHelper.TABELA_KATEGORIJA, null, selection, null, null, null, null);
            long idKategorije;
            if(cursor.getCount() != 0) {
                cursor.moveToFirst();
                idKategorije = cursor.getLong(cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_ID));
                knjige = new ArrayList<Knjiga>();
                knjige.addAll(bazaOpenHelper.knjigeKategorije(idKategorije));
                adapter = new MyArrayAdapter(view.getContext(), R.layout.element_liste, knjige);
                lista.setAdapter(adapter);
            }
            cursor.close();
        } else if(getArguments().containsKey("autor")){
            BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity().getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);
            SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
            String selection = BazaOpenHelper.AUTOR_IME + "='" + autor + "'";

            Cursor cursor = db.query(BazaOpenHelper.TABELA_AUTOR, null, selection, null, null, null, null);

            cursor.moveToFirst();
            long idAutora = cursor.getLong(cursor.getColumnIndex(BazaOpenHelper.AUTOR_ID));

            knjige = new ArrayList<Knjiga>();
            knjige.addAll(bazaOpenHelper.knjigeAutora(idAutora));
            adapter = new MyArrayAdapter(view.getContext(),R.layout.element_liste, knjige);
            lista.setAdapter(adapter);

            cursor.close();
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {

            klik = (OnClicks) getActivity();
        } catch(ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }

        final Button povratak = (Button)getView().findViewById(R.id.dPovratak);

        povratak.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                klik.onClickPovratak();

            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                lista.getChildAt(position-lista.getFirstVisiblePosition()).setBackgroundColor(getResources().getColor(R.color.colorItem));
                knjige.get(position).setColored(true);

                String nazivKnjige = knjige.get(position).getNaziv();
                ContentValues updatedValues = new ContentValues();
                updatedValues.put(BazaOpenHelper.KNJIGA_PREGLEDANA, 1);

                BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity().getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);
                SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
                String where = BazaOpenHelper.KNJIGA_NAZIV + "='" + nazivKnjige + "'";

                db.update(BazaOpenHelper.TABELA_KNJIGA, updatedValues, where, null);

            }
        });

    }

    public interface OnClicks {

        public void onClickPovratak();

    }

}
