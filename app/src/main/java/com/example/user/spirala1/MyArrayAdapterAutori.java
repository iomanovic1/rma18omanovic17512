package com.example.user.spirala1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by user on 8.4.2018.
 */

public class MyArrayAdapterAutori extends ArrayAdapter<Autor> {
    int resource;

    public MyArrayAdapterAutori(Context context, int _resource, List<Autor> items) {
        super(context, _resource, items);
        resource = _resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }
        Autor autor = getItem(position);

        TextView imeAutora = (TextView) newView.findViewById(R.id.eImeIPrezime);
        TextView brojKnjiga = (TextView) newView.findViewById(R.id.eBrojKnjiga);

        String kolikoKnjiga = getContext().getResources().getString(R.string.broj_knjiga) + Integer.toString(autor.getBrojKnjiga());

        imeAutora.setText(autor.getImeIPrezime());
        brojKnjiga.setText(kolikoKnjiga);

        return newView;
    }
}
