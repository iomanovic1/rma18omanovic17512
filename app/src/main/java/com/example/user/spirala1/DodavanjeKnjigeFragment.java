package com.example.user.spirala1;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by user on 7.4.2018.
 */

public class DodavanjeKnjigeFragment extends Fragment {

    OnClicksDodajKnjigu klik;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dodavanje_knjige_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Spinner spinner = (Spinner)getView().findViewById(R.id.sKategorijaKnjige);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Kolekcije.kategorije);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);



        klik = (OnClicksDodajKnjigu) getActivity();


        Button dugmeNadjiSliku = (Button)getView().findViewById(R.id.dNadjiSliku);

        dugmeNadjiSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                klik.onClickNadjiSliku();

            }
        });

        final Button dugmeUpisiKnjigu = (Button)getView().findViewById(R.id.dUpisiKnjigu);

        dugmeUpisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ImageView naslovnaStr = (ImageView) getActivity().findViewById(R.id.naslovnaStr);

                final TextView imeAutora = (TextView) getActivity().findViewById(R.id.imeAutora);

                final TextView nazivKnjige = (TextView) getActivity().findViewById(R.id.nazivKnjige);

                if(spinner.getCount() == 0) {
                    return;
                }
                if(imeAutora.getText().length() == 0 && nazivKnjige.getText().length() == 0) {
                } else if(imeAutora.getText().length() == 0) {
                } else if(nazivKnjige.getText().length() == 0) {

                } else
                {
                    if(naslovnaStr.getDrawable() == null)
                        naslovnaStr.setImageBitmap(((BitmapDrawable)getResources().getDrawable(R.drawable.book_launch_graphic2)).getBitmap());

                    Knjiga knjiga = new Knjiga(((BitmapDrawable)naslovnaStr.getDrawable()).getBitmap(), imeAutora.getText().toString(), nazivKnjige.getText().toString(), spinner.getSelectedItem().toString());
                    BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity().getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);

                    if(bazaOpenHelper.dodajKnjigu(knjiga) != -1) {
                        Kolekcije.Knjige.add(knjiga);
                        naslovnaStr.setImageBitmap(null);

                        String trenutniAutor = imeAutora.getText().toString();
                        Boolean postoji = false;
                        for(Autor a : Kolekcije.Autori ) {
                            if(a.getImeIPrezime().equals(trenutniAutor)) {
                                postoji = true;
                                a.dodajKnjigu(knjiga.getNaziv());
                                a.povecajBrojKnjiga();
                                break;
                            }
                        }

                        if(!postoji) {
                            Kolekcije.Autori.add(new Autor(trenutniAutor));
                        } else {
                        }
                        imeAutora.setText("");
                        nazivKnjige.setText("");
                        spinner.setSelection(0);
                    } else {

                    }

                }

            }
        });

        final Button dugmePonisti = (Button)getView().findViewById(R.id.dPonisti);

        dugmePonisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klik.onClickPonisti();
            }
        });
    }

    public long dodajKnjigu(Knjiga knjiga) {

        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity().getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);

        String[] koloneRezulat = new String[]{BazaOpenHelper.KATEGORIJA_ID};
        String where = BazaOpenHelper.KATEGORIJA_NAZIV + " = " + knjiga.getKategorija();
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.TABELA_KATEGORIJA, koloneRezulat, where, null, null, null, null);

        String idKategorije = cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_ID));
        cursor.close();

        ContentValues noveVrijednosti = new ContentValues();
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_NAZIV, knjiga.getNaziv());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_OPIS, knjiga.getOpis());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_BROJ_STRANICA, knjiga.getBrojStranica());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_ID_WEB_SERVIS, knjiga.getId());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_ID_KATEGORIJE, idKategorije);
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_SLIKA, knjiga.getSlika().toString());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_PREGLEDANA, knjiga.getColored());

        long rowIdKnjige = db.insert(BazaOpenHelper.TABELA_KNJIGA, null, noveVrijednosti);

        if(rowIdKnjige == -1)
            return -1;

        if(knjiga.getAutori().size() != 0) {
            for(int i = 0; i < knjiga.getAutori().size(); i++) {
                Autor autor = knjiga.getAutori().get(i);

                ContentValues noveVrijednostiAutora = new ContentValues();
                noveVrijednostiAutora.put(BazaOpenHelper.AUTOR_IME, autor.getImeIPrezime());

                long rowIdAutora = db.insert(BazaOpenHelper.TABELA_AUTOR, null, noveVrijednostiAutora);

                if(rowIdAutora != -1) {

                    ContentValues noveVrijednostiAutorstva = new ContentValues();
                    noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_AUTORA, rowIdAutora);
                    noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_KNJIGE, rowIdKnjige);

                    long rowIdAutorstva = db.insert(BazaOpenHelper.TABELA_AUTORSTVO, null, noveVrijednostiAutorstva);

                }
            }
        } else if(!knjiga.getAutor().contains(";")) {
            String imeAutora = knjiga.getAutor();

            ContentValues noveVrijednostiAutora = new ContentValues();
            noveVrijednostiAutora.put(BazaOpenHelper.AUTOR_IME, imeAutora);

            long rowIdAutora = db.insert(BazaOpenHelper.TABELA_AUTOR, null, noveVrijednostiAutora);

            if(rowIdAutora != -1) {

                ContentValues noveVrijednostiAutorstva = new ContentValues();
                noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_AUTORA, rowIdAutora);
                noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_KNJIGE, rowIdKnjige);

                long rowIdAutorstva = db.insert(BazaOpenHelper.TABELA_AUTORSTVO, null, noveVrijednostiAutorstva);

            }
        }

        return rowIdKnjige;
    }

    public interface OnClicksDodajKnjigu {

        public void onClickNadjiSliku();
        public void onClickPonisti();

    }
}
