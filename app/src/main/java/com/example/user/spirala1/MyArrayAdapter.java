package com.example.user.spirala1;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by user on 27.3.2018.
 */

public class MyArrayAdapter extends ArrayAdapter<Knjiga> {

    int resource;

    public MyArrayAdapter(Context context, int _resource, List<Knjiga> items) {
        super(context, _resource, items);
        resource = _resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }
        final Knjiga knjiga = getItem(position);

        ImageView slika = (ImageView) newView.findViewById(R.id.eNaslovna);
        TextView naziv = (TextView) newView.findViewById(R.id.eNaziv);
        TextView autor = (TextView) newView.findViewById(R.id.eAutor);
        ConstraintLayout relativeLayout = (ConstraintLayout) newView.findViewById(R.id.relativeLayout);
        TextView brojStranica = (TextView) newView.findViewById(R.id.eBrojStranica);
        TextView datumObjavljivanja = (TextView) newView.findViewById(R.id.eDatumObjavljivanja);
        TextView opis = (TextView) newView.findViewById(R.id.eOpis);

        if(knjiga.getColored())
            relativeLayout.setBackgroundColor(getContext().getResources().getColor(R.color.colorItem));

        slika.setImageBitmap(knjiga.getNaslovnaStrana());
        naziv.setText(knjiga.getNaziv());
        autor.setText(knjiga.getAutor());
        String brojStranicaKnjige;
        if(knjiga.getBrojStranica() != 0) {
            brojStranicaKnjige = String.valueOf(knjiga.getBrojStranica());
        } else {
            brojStranicaKnjige =  "Nepoznato";
        }
        brojStranica.setText(brojStranicaKnjige);

        datumObjavljivanja.setText(knjiga.getDatumObjavljivanja());
        opis.setText(knjiga.getOpis());

        final Button dugmePreporuci = (Button) newView.findViewById(R.id.dPreporuci);

        dugmePreporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle arguments = new  Bundle();
                arguments.putParcelable("knjiga", knjiga);
                //arguments.putStringArrayList("dodajKnjigu", Biblioteka.Kategorije);
                FragmentPreporuci fp = new FragmentPreporuci();
                fp.setArguments(arguments);

                KategorijeAkt ka = (KategorijeAkt) newView.getContext();
                ka.getFragmentManager().beginTransaction().replace(R.id.mjestoF1, fp,"PreporuciKnjigu").addToBackStack(null).commit();

            }
        });

        return newView;
    }
}

