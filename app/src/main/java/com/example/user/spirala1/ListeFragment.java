package com.example.user.spirala1;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by user on 6.4.2018.
 */

public class ListeFragment extends Fragment {
    ArrayList<Autor> autori;
    OnClick klik;
    ArrayList<String> kategorije;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lista_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Button dugmeDodajKnjigu = (Button)getView().findViewById(R.id.dDodajKnjigu);
        Button dugmeKategorije = (Button)getView().findViewById(R.id.dKategorije);
        Button dugmeAutori = (Button)getView().findViewById(R.id.dAutori);

        if (getArguments().containsKey("kategorije")) {

            kategorije = getArguments().getStringArrayList("kategorije");

            ListView lv = (ListView) getView().findViewById(R.id.listaKategorija);
            ArrayAdapter<String> aa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, kategorije);
            lv.setAdapter(aa);

            klik = (OnClick) getActivity();

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    klik.onItemClickedKategorije(position);
                }
            });



            Button dugmePretraga = (Button)getView().findViewById(R.id.dPretraga);

            dugmePretraga.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    klik.onClickPretraga();
                }
            });

            final Button dugmeDodajKategoriju = (Button)getView().findViewById(R.id.dDodajKategoriju);
            final EditText tekst = (EditText)getView().findViewById(R.id.tekstPretraga);

            dugmeDodajKategoriju.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity().getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);

                    if(bazaOpenHelper.dodajKategoriju(tekst.getText().toString()) != -1) {
                        klik.onClickDodajKategoriju();
                    } else {
                    }
                }
            });



            Button dugmeDodajKnjiguOnline = (Button)getView().findViewById(R.id.dDodajOnline);

            dugmeDodajKnjiguOnline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //poziva se implementirana metoda početne aktivnosti iz interfejsa OnItemClick
                    //kao parametar se prosljeđuje pozicija u ListView-u na koju je korisnik kliknuo
                    klik.onClickDodajKnjiguOnline();
                }
            });


            tekst.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                    if(dugmeDodajKategoriju.isEnabled()) {
                        dugmeDodajKategoriju.setEnabled(false);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                }
            });

        } else if(getArguments().containsKey("autori")) {

            autori = Kolekcije.Autori;


            ListView lv = (ListView)getView().findViewById(R.id.listaKategorija);

            final EditText tekstPretraga = (EditText)getActivity().findViewById(R.id.tekstPretraga);
            final Button pretraga = (Button)getActivity().findViewById(R.id.dPretraga);
            final Button dodajKategoriju = (Button)getActivity().findViewById(R.id.dDodajKategoriju);

            pretraga.setVisibility(View.GONE);
            dodajKategoriju.setVisibility(View.GONE);
            tekstPretraga.setVisibility(View.GONE);

            MyArrayAdapterAutori aa = new MyArrayAdapterAutori(getActivity(), R.layout.element_liste_autori, autori);


            lv.setAdapter(aa);

            klik = (OnClick) getActivity();

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    klik.onItemClickedAutori(position);
                }
            });

        }

        dugmeDodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klik.onClickDodajKnjigu();
            }
        });

        dugmeKategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klik.onClickKategorije();
            }
        });

        dugmeAutori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klik.onClickAutori();
            }
        });
    }




    public interface OnClick {
        public void onClickPretraga();
        public void onClickKategorije();
        public void onClickAutori();
        public void onClickDodajKategoriju();
        public void onClickDodajKnjigu();
        public void onItemClickedKategorije(int pos);
        public void onItemClickedAutori(int pos);
        public void onClickDodajKnjiguOnline();
    }
}
