package com.example.user.spirala1;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class FragmentOnline extends Fragment implements MyResultReceiver.Receiver, DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone {

    OnClicksDodajOnline klik;
    ArrayList<Knjiga> onlineKnjige;
    ArrayList<String> naziviOnlineKnjiga;
    ArrayAdapter<String> knjigeAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        onlineKnjige = new ArrayList<Knjiga>();
        naziviOnlineKnjiga = new ArrayList<String>();
        return inflater.inflate(R.layout.fragment_online, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Spinner spinnerKnjige = (Spinner)getView().findViewById(R.id.sRezultat);

        knjigeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, naziviOnlineKnjiga);
        knjigeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerKnjige.setAdapter(knjigeAdapter);



        final Spinner spinner = (Spinner)getView().findViewById(R.id.sKategorije);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Kolekcije.kategorije);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        try {

            klik = (OnClicksDodajOnline) getActivity();
        } catch(ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnItemClick");
        }



        final Button dugmePretraga = (Button)getView().findViewById(R.id.dRun);

        dugmePretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final EditText text = (EditText)getView().findViewById(R.id.tekstUpit);
                final String tekst = text.getText().toString();

                if(tekst.length() < 6) {

                    String[] rijeci = tekst.split(";");
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(rijeci);

                }else if(tekst.substring(0,6).equals("autor:")) {
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone) FragmentOnline.this).execute(tekst.substring(6));
                } else if(tekst.length() < 9) {
                    String[] rijeci = tekst.split(";");
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(rijeci);

                } else if(tekst.substring(0,9).equals("korisnik:")) {
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);

                    MyResultReceiver mReceiver = new MyResultReceiver(new Handler());
                    mReceiver.setReceiver(FragmentOnline.this);

                    intent.putExtra("idKorisnika", tekst.substring(9));
                    intent.putExtra("receiver", mReceiver);

                    getActivity().startService(intent);

                } else {
                    String[] rijeci = tekst.split(";");
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(rijeci);
                }

            }
        });

        final Button dugmeDodajKnjigu = (Button)getView().findViewById(R.id.dAdd);

        dugmeDodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(spinner.getCount() == 0) {
                    return;
                }

                if(spinnerKnjige.getCount() == 0) {
                    return;
                }

                Knjiga k = null;
                String knjiga = spinnerKnjige.getSelectedItem().toString();
                for(int i = 0; i < onlineKnjige.size(); i++) {
                    if(onlineKnjige.get(i).getNaziv().equals(knjiga)) {
                        k = onlineKnjige.get(i);
                        break;
                    }
                }

                if(k != null) {
                    k.setNaslovnaStr(((BitmapDrawable)getResources().getDrawable(R.drawable.book_launch_graphic2)).getBitmap());
                    k.setKategorija(spinner.getSelectedItem().toString());
                    Boolean postoji = false;
                    for(int i = 0; i < Kolekcije.Knjige.size(); i++) {
                        if(k.getId().equals(Kolekcije.Knjige.get(i).getId())) {
                            postoji = true;
                            break;
                        }
                    }
                    if(!postoji) {
                        Kolekcije.Knjige.add(k);
                        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity().getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);
                        bazaOpenHelper.dodajKnjigu(k);
                    } else {
                        Toast.makeText(getActivity().getBaseContext(), getResources().getString(R.string.toast_knjiga_vec_dodana), Toast.LENGTH_SHORT).show();
                        return;
                    }

                }

                for(int i = 0; i < k.getAutori().size(); i++) {
                    Boolean postoji = false;
                    Autor a = k.getAutori().get(i);
                    for(int j = 0; j < Kolekcije.Autori.size(); j++) {
                        if(a.getImeIPrezime().equals(Kolekcije.Autori.get(j).getImeIPrezime())) {
                            postoji = true;
                            Kolekcije.Autori.get(j).povecajBrojKnjiga();
                            Kolekcije.Autori.get(j).dodajKnjigu(k.getId());
                            break;
                        }
                    }
                    if(!postoji)
                        Kolekcije.Autori.add(a);
                }
            }
        });

        final Button dugmePovratak = (Button)getView().findViewById(R.id.dPovratak);
        dugmePovratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                klik.onClickPonisti();
            }
        });

    }

    public interface OnClicksDodajOnline {
        public void onClickPonisti();
    }

    @Override
    public void onDohvatiDone(ArrayList<Knjiga> k) {
        onlineKnjige.clear();
        onlineKnjige.addAll(k);
        naziviOnlineKnjiga.clear();
        for (Knjiga book : onlineKnjige) {
            naziviOnlineKnjiga.add(book.getNaziv());
        }
        knjigeAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> k) {
        onDohvatiDone(k);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case KnjigePoznanika.STATUS_START:
                Toast.makeText(getActivity(), "Proces započet", Toast.LENGTH_LONG).show();
                break;
            case KnjigePoznanika.STATUS_FINISH:
                Toast.makeText(getActivity(), "Proces završen", Toast.LENGTH_LONG).show();
                ArrayList<Knjiga> k = resultData.getParcelableArrayList("result");
                onDohvatiDone(k);
                break;
            case KnjigePoznanika.STATUS_ERROR:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                break;
        }
    }

    public long dodajKnjigu(Knjiga knjiga) {

        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity().getApplicationContext(), BazaOpenHelper.DATABASE_NAME, null, 1);

        String[] koloneRezulat = new String[]{BazaOpenHelper.KATEGORIJA_ID};
        String where = BazaOpenHelper.KATEGORIJA_NAZIV + " = " + knjiga.getKategorija();
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.TABELA_KATEGORIJA, koloneRezulat, where, null, null, null, null);

        String idKategorije = cursor.getString(cursor.getColumnIndex(BazaOpenHelper.KATEGORIJA_ID));
        cursor.close();

        ContentValues noveVrijednosti = new ContentValues();
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_NAZIV, knjiga.getNaziv());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_OPIS, knjiga.getOpis());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_DATUM_OBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_BROJ_STRANICA, knjiga.getBrojStranica());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_ID_WEB_SERVIS, knjiga.getId());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_ID_KATEGORIJE, idKategorije);
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_SLIKA, knjiga.getSlika().toString());
        noveVrijednosti.put(BazaOpenHelper.KNJIGA_PREGLEDANA, knjiga.getColored());


        long rowIdKnjige = db.insert(BazaOpenHelper.TABELA_KNJIGA, null, noveVrijednosti);

        if(rowIdKnjige == -1)
            return -1;
        if(knjiga.getAutori().size() != 0) {
            for(int i = 0; i < knjiga.getAutori().size(); i++) {
                Autor autor = knjiga.getAutori().get(i);

                ContentValues noveVrijednostiAutora = new ContentValues();
                noveVrijednostiAutora.put(BazaOpenHelper.AUTOR_IME, autor.getImeIPrezime());

                long rowIdAutora = db.insert(BazaOpenHelper.TABELA_AUTOR, null, noveVrijednostiAutora);

                if(rowIdAutora != -1) {

                    ContentValues noveVrijednostiAutorstva = new ContentValues();
                    noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_AUTORA, rowIdAutora);
                    noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_KNJIGE, rowIdKnjige);

                    long rowIdAutorstva = db.insert(BazaOpenHelper.TABELA_AUTORSTVO, null, noveVrijednostiAutorstva);

                }
            }
        } else if(!knjiga.getAutor().contains(";")) {
            String imeAutora = knjiga.getAutor();

            ContentValues noveVrijednostiAutora = new ContentValues();
            noveVrijednostiAutora.put(BazaOpenHelper.AUTOR_IME, imeAutora);

            long rowIdAutora = db.insert(BazaOpenHelper.TABELA_AUTOR, null, noveVrijednostiAutora);

            if(rowIdAutora != -1) {

                ContentValues noveVrijednostiAutorstva = new ContentValues();
                noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_AUTORA, rowIdAutora);
                noveVrijednostiAutorstva.put(BazaOpenHelper.AUTORSTVO_ID_KNJIGE, rowIdKnjige);

                long rowIdAutorstva = db.insert(BazaOpenHelper.TABELA_AUTORSTVO, null, noveVrijednostiAutorstva);

            }
        }
        return rowIdKnjige;
    }

}
